class WelcomeController < ApplicationController
  def index
    @gif = Gif.by_date_desc.first
  end
end