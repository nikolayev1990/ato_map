ActiveAdmin.register Map do
  permit_params :date, :image

  index do
    selectable_column
    id_column
    column :date
    column :image
    column :created_at
    actions
  end

  action_item :load_last_maps, only: [:index] do
    link_to 'Load New Maps', load_last_admin_maps_path, method: :post, data: { confirm: "Are you sure?" }
  end

  collection_action :load_last, method: :post do
    MapsService.new.parse_last_maps
    redirect_to admin_maps_path, notice: "New maps loaded"
  end

  filter :date
  filter :created_at

  form do |f|
    f.inputs "Map Details" do
      f.input :date
      f.input :image, hint: image_tag(f.object.image_url, width: "100%")
    end
    f.actions
  end

end
