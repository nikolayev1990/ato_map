ActiveAdmin.register Gif do
  permit_params :date, :image

  index do
    selectable_column
    id_column
    column :date
    column :image
    column :created_at
    actions
  end

  action_item :create_todays_gif, only: [:index] do
    link_to 'Create Gif for Today', create_new_admin_gifs_path, method: :post, data: { confirm: "Are you sure?" }
  end

  collection_action :create_new, method: :post do
    GifsService.new.create_gif_for_today
    redirect_to admin_gifs_path, notice: "New gif created"
  end

  filter :date
  filter :created_at

  form do |f|
    f.inputs "Gif Details" do
      f.input :date
      f.input :image, hint: (image_tag(f.object.image_url, width: "100%") if f.object.image_url)
    end
    f.actions
  end

end
