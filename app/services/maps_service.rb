require 'nokogiri'
require 'open-uri'

class MapsService
  def initialize(count = 30)
    @url = "https://ukr.media/carta-ato/?os=%{page}"
    @count = count
  end

  def parse_last_maps
    page = 0
    while @count > 0
      page += 1
      parse(@url % { page: page })
    end
  end

  private

  def parse(url)
    doc = Nokogiri::HTML(open(url))

    doc.css('.col-a .item.article4').each_with_index do |item, index|
      break if @count.zero?
      @count -= 1

      map = Map.find_or_create_by(date: date(item)) do |m|
        m.remote_image_url = image_url(item)
      end
    end
  end

  def date(item)
    date = item.at_css(".timestamp span").text.strip
    if date =~ /сьогодні/
      return Date.today
    end

    match = date.match /(\d+)\s([[:alpha:]]+)\s(\d+)/
    month = {
      "Січня" => 1,
      "Лютого" => 2,
      "Березня" => 3,
      "Квітня" => 4,
      "Травня" => 5,
      "Червня" => 6,
      "Липня" => 7,
      "Серпня" => 8,
      "Вересня" => 9,
      "Жовтня" => 10,
      "Листопада" => 11,
      "Грудня" => 12
    }[match[2]]
    Date.new match[3].to_i, month, match[1].to_i
  end

  def image_url(item)
    url = item.at_css(".form-url a").attr "href"
    image_url = Nokogiri::HTML(open(url)).at_css('.b-topic-layout img').attr "src"
    image_url.gsub /^\/\//, "https://"
  end
end