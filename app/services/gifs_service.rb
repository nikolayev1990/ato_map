class GifsService
  def create_gif_for_today
    date = Date.today
    Gif.find_or_create_by(date: date) do |gif|
      temp_file data(date) do |file|
        gif.image = file
      end
    end
  end

  private

  def data(date)
    gif = Magick::ImageList.new *images(date)
    gif.delay = 20 # delay 1/5 of a second between images.
    gif.to_blob { |attrs| attrs.format = 'gif' }
  end

  def temp_file data, &block
    file = Tempfile.new('foo', encoding: 'ascii-8bit')
    file.write data
    file.close
    yield file
    file.unlink
  end

  def images(date)
    Map.by_date.where(Map.arel_table["date"].lteq(date)).limit(30).map do |map|
      case map.image._storage.name.demodulize
      when "Fog"
        map.image.url
      when "File"
        map.image.path
      end
    end
  end
end