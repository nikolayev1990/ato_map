class Gif < ApplicationRecord
  mount_uploader :image, ImageUploader

  scope :by_date_desc, -> { order(date: :desc) }
end
