class Map < ApplicationRecord
  mount_uploader :image, ImageUploader

  scope :by_date, -> { order(:date) }
end
