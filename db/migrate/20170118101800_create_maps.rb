class CreateMaps < ActiveRecord::Migration[5.0]
  def change
    create_table :maps do |t|
      t.date :date, null: false
      t.string :image

      t.timestamps
    end
  end
end
