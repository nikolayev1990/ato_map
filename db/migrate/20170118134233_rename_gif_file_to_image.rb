class RenameGifFileToImage < ActiveRecord::Migration[5.0]
  def change
    rename_column :gifs, :file, :image
  end
end
