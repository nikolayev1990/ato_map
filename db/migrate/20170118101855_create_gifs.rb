class CreateGifs < ActiveRecord::Migration[5.0]
  def change
    create_table :gifs do |t|
      t.date :date, null: false
      t.string :file

      t.timestamps
    end
  end
end
